import json
import datetime
from datetime import date

from app import logging

logger = logging.getLogger(__name__)

months = {
    1: 'enero', 2: 'febrero', 3: 'marzo', 4: 'abril',
    5: 'mayo', 6: 'junio', 7: 'julio', 8: 'agosto',
    9: 'septiembre', 10: 'octubre', 11: 'noviembre', 12: 'diciembre'
}

days = ["lunes", "martes", "miercoles", "jueves", "viernes"]

calendar = None
user_timetable = None

def load_resources(user_id):    
    calendar_path = "data/school_calendar.json"
    user_timetable_path = "data/{}.json".format(user_id)

    global calendar, user_timetable

    # Load the school calendar
    with open(calendar_path, "r") as f:
        calendar = json.load(f)
    
    # Load the user calendar
    try:
        with open(user_timetable_path, "r") as f:
            user_timetable = json.load(f)
    except FileNotFoundError:
        return False
    
    return True


def get_class_day(when, *args, **kwargs):
    """  """   
    target_date = date.today()
    # logger.debug(when)
    if(when == "today"):
        
        valid_day, day_name, week_type = get_real_school_day(target_date)
        day_number = target_date.day
        month_number = target_date.month


        return (valid_day, day_number, month_number, day_name, week_type)

    elif(when == "next"):
        # Search for the next day with class
        target_date = date.today()
        attempts = 0
        valid_day = False
        while(not valid_day and attempts < 30):
            target_date = target_date + datetime.timedelta(days=1)
            valid_day, day_name, week_type = get_real_school_day(target_date)
            day_number = target_date.day
            month_number = target_date.month

            # If the target date there is no class the target date
            if(not valid_day):
                attempts += 1

        return (valid_day, day_number, month_number, day_name, week_type)

    else:        
        day_number = kwargs["day"]
        month_number = kwargs["month"]
        target_date = date(day=int(day_number), month=int(month_number), year=date.today().year)
        valid_day, day_name, week_type = get_real_school_day(target_date)

        return (valid_day, day_number, month_number, day_name, week_type)

def get_real_school_day(target_date):
    """  """
    day_name = " "
    week_type = " "
    valid_day = True

    try:            
        day_name = calendar[months[target_date.month]][str(target_date.day)]["dia"]        
        week_type = calendar[months[target_date.month]][str(target_date.day)]["semana"]
    except KeyError as e:
        valid_day = False

    return (valid_day, day_name, week_type)

def get_classes(valid_day, day_number, month_number, day_name, week_type):
    """  """
    class_info = {        
        "theory_class_list": [],
        "practice_sessions_list": [],
        "day": day_number,
        "month": month_number,
        "week": week_type
    }    

    # Get all the teorical classes, it key not found there are no teorical clases
    # that day
    try:
        for i in user_timetable["clases_teoria"][day_name]["asignaturas"]:            
            class_info["theory_class_list"].append({
                "name": i["nombre"],
                "room": i["aula"],
                "type": i["tipo"],
                "from": i["hora_inicio"],
                "until": i["hora_fin"]
            })
            
    except KeyError:
        pass
    
    # Get all the practical classes, if key not found there are no practical
    # classes that day
    try:
        for i in user_timetable["clases_practicas"][day_name][week_type]:            
            class_info["practice_sessions_list"].append({
                "name": i["nombre"],
                "room": i["aula"],
                "type": i["tipo"],
                "from": i["hora_inicio"],
                "until": i["hora_fin"]
            })            
    except KeyError:
        pass
    
    logger.debug("clases {}".format(class_info))        

    return class_info

def get_message(class_info, when):
    today_date = date.today()

    theory_clases_list = class_info["theory_class_list"]
    practice_sessions_list = class_info["practice_sessions_list"]    
    day = class_info["day"]
    month = class_info["month"]
    week = class_info["week"]    

    if(when == "today"):
        message = (
            "Hoy: <b>{}/{}</b>\n".format(today_date.day, today_date.month) +
            "Semana: <b>{}</b>\n\n".format(week)
        )

    elif(when == "next"):
        message = (
            "Hoy: <b>{}/{}</b>\n".format(today_date.day, today_date.month) +
            "Próximo día de clase: <b>{}/{}</b>\n".format(day, month) +
            "Semana: <b>{}</b>\n\n".format(week)
        )
    else:
        message = (
            "El día: <b>{}/{}</b>\n".format(day, month) +
            "Semana: <b>{}</b>\n\n".format(week)
        )

    if(len(theory_clases_list) + len(practice_sessions_list) == 0):        
        message = "<b>No tienes clases</b>"
    else:
        message += "<b>Teoria/Problemas:</b>\n"
        if(len(theory_clases_list) > 0):
            for theory_class in theory_clases_list:
                message += (
                    "\t<b>Asignatura:</b> {}\n".format(theory_class["name"]) +
                    "\t<b>Aula:</b> {}\n".format(theory_class["room"]) +
                    "\t<b>Tipo:</b> {}\n".format(theory_class["type"]) +
                    "\t<b>Empieza a las:</b> {}\n".format(theory_class["from"]) +
                    "\t<b>Acaba a las:</b> {}\n\n".format(theory_class["until"])
                )
        else:
            message += "\t\t<i>No tienes clases teoricas ni problemas hoy</i>"

        message += "\n<b>Practicas:</b>\n" 
        if(len(practice_sessions_list) > 0):
            for practice_session in practice_sessions_list:
                message += (
                    "\t<b>Asignatura:</b> {}\n".format(practice_session["name"]) +
                    "\t<b>Aula:</b> {}\n".format(practice_session["room"]) +
                    "\t<b>Tipo:</b> {}\n".format(practice_session["type"]) +
                    "\t<b>Empieza a las:</b> {}\n".format(practice_session["from"]) +
                    "\t<b>Acaba a las:</b> {}\n".format(practice_session["until"])
                )
        else:
            message += "\t\t<i>No tienes clases practicas hoy</i>"
    
    return message

