import os
import json

from app import logging
from app import middleware

logger = logging.getLogger(__name__)

@middleware.check_user_pipeline
def upload_timetable(bot, update):   
    """ Function than it's executed when a file is received. It should be executed
        only after a user wants to upload a timetable 
    """
    user_id = update.effective_message.chat_id

    # If user is not on a command pipeline or if it's on a different than
    # "attempting to upload timetable file", do not execute anything
    if(user_id not in middleware.user_pipelines or 
        middleware.user_pipelines[user_id] != middleware.user_available_states[0]):
        return 

    # Get the file size (in bytes) and checks if its too big
    # Allow users to upload a 100KB maximum file
    file_size = update.message.document.file_size
    if(int(file_size) >= 100000):
        message = (
            "Fichero demasiado grande.\n"
            "Vuelve a intentarlo"
        )
        bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
        return

    tmp_file_path = "data/tmp/{}_tmp.json".format(user_id)    
    user_timetable_path = "data/{}.json".format(user_id)
    
    # Downloads the file
    file_id = update.message.document.file_id    
    newFile = bot.get_file(file_id)
    newFile.download(tmp_file_path)
    
    # Checks if the json file is valid
    with open(tmp_file_path, "r") as f:        
        try:
            timetable_json = json.load(f)
        except json.decoder.JSONDecodeError:            
            message = (
                "El fichero con el horario está mal formado.\n"
                "Porfavor, revisalo y vuelve a enviarlo"
            )
            bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
            return
        except Exception:
            message = (
                "Formato del fichero no valido\n"
                "Porfavor, asegurate de que envias el fichero correcto en "
                "formato json"
            )
            bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
            return
    
    # If file is valid, save it
    os.rename(tmp_file_path, user_timetable_path)    

    # End the user upload timetable pipeline
    del middleware.user_pipelines[user_id]

    message = (
        "Horario subido con éxito, ahora puedes consultar tu horario"
        "introduciendo *hoy*, *mañana* o el día y el mes en el formato *dia/mes*"
        )
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')