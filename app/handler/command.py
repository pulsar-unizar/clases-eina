import os 
import time

from app import logging
from app import middleware

logger = logging.getLogger(__name__)

@middleware.check_user_pipeline
def start(bot, update):
    """ Function than it's executed when the command '/start' is received """
    
    logger.debug("Start command received")
    user_id = update.effective_message.chat_id    
    message = """  """    
    
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')

def help(bot, update):
    """ Function that it's executed when the command '/help' is received """
    
    logger.debug("Help command received")
    user_id = update.effective_message.chat_id
    help_message = (
        "Este bot dispone de varios comandos para la creación, modificación y "
        "eliminación del horario \n\n"
        "• Con el comando /add podrás crear tu horario. Se te "    
        "enviará un fichero en formato JSON que tendrás que modificar "
        "introduciendo tu horario y volver a mandarlo para guardarlo.\n\n"
        "• Con el comando /download podrás descargar el horario que "
        "tengas subido para compartirlo con los compañeros o modificarlo y "
        "volver a subirlo\n\n"
        "• Con el comando /remove se borrará el horario que tengas "
        "subido, esto lo puedes usar en caso de que no quieras utilizar más "
        "este bot\n\n"
        "Para poder consultar tu horario (una vez creado y subido) simplemente "
        "tendrás que introducir las palabras clave <b>(h)oy</b> o <b>(s)iguiente</b> "
        "para saber el horario de hoy o del siguiente día de clase. Incluso podrás "
        "introducir un día y un mes en el formato <b>dd/mm</b> para saber "
        "las clases de un día en concreto.\n\n"
        "Existe un comando especial /cancel que sirve precisamente para "
        "cancelar operaciones como la de crear horario o la del borrado."
    )

    bot.send_message(chat_id=user_id, text=help_message, parse_mode='html')


def about(bot, update):
    """ Function that it's executed when the command '/about' is received """
    
    logger.debug("About command received")
    user_id = update.effective_message.chat_id
    about_message = """ Este bot ha sido creado por *Púlsar*, la Asociacíon de \
Software Libre de la Universidad de Zaragoza, bajo la licencia *GPLv3*.

Puedes contribuir a este proyectoen la siguiente dirección: \
https://gitlab.com/pulsar-unizar/clases-eina"""

    bot.send_message(chat_id=user_id, text=about_message, parse_mode='markdown')



def cancel(bot, update):
    """ Function than it's executed when the command '/cancel' is received 
        It's used to cancel the user pipeline. So, no check_pipeline is needed.
    """
    user_id = update.effective_message.chat_id
    if(user_id in middleware.user_pipelines):
        del middleware.user_pipelines[user_id]

@middleware.send_upload_document_action
@middleware.check_user_pipeline
def add_timetable(bot, update):
    """ Function than it's executed when the command '/add' is 
        received.
        
        It will begin the upload timetable pipeline. The next step is receiving
        the file
    """
    
    user_id = update.effective_message.chat_id

    # Checks the user state, if has no begun a pipeline, he will begin the
    # add_timetable pipeline now
    if(user_id not in middleware.user_pipelines):
        middleware.user_pipelines[user_id] = middleware.user_available_states[0]    

    message = (
        "Ten esta plantilla, rellénala con tu horario de teoría y "
        "practicas, y vuelve a mandarlo. Si no sabes cómo hacerlo, "
        "las instrucciones se encuentran al principio del fichero. "
        "¡Te será mas fácil hacerlo desde un ordenador!\n"        
        )
    
    # Checks if the file already exists
    user_timetable_path = "data/{}.json".format(user_id)
    if(os.path.isfile(user_timetable_path)):
        message += (
            "\n<b>IMPORTANTE: El fichero ya existe, si no quieres sobreescribir tu "
            "horario actual, puedes cancelar la operación ejecutando el comando</b> " 
            "/cancel"
        )
    # Sends the template to the user, so he can modify with his own data and
    # then sends back
    template_timetable_path = "data/plantilla.json".format(user_id)

    with open(template_timetable_path, 'rb') as f:
        bot.send_document(chat_id=user_id, document=f, caption=message, parse_mode='html')        

@middleware.check_user_pipeline
def download_timetable(bot, update):
    """ Function than it's executed when the command '/download' 
        is received.
    """
    user_id = update.effective_message.chat_id    

    user_timetable_path = "data/{}.json".format(user_id)

    try:
        with open(user_timetable_path, 'rb') as f:
            bot.send_document(chat_id=user_id, document=f, caption="Horario")
    except FileNotFoundError:
        message = "No tienes ningún horario creado!"
        bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')

@middleware.check_user_pipeline
def delete_timetable(bot, update):
    """ Function than it's executed when the command '/remove' 
        is received.

        It will begin the delete file pipeline. The next step is a confirmation
    """
    user_id = update.effective_message.chat_id

    # Checks if the file already exists
    user_timetable_path = "data/{}.json".format(user_id)
    if(not os.path.isfile(user_timetable_path)):
        message = "No tienes ningún horario creado!"
        bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
        return

    # Adds the user to the "attempting to delete timetable file" pipeline    
    if(user_id not in middleware.user_pipelines):
        middleware.user_pipelines[user_id] = middleware.user_available_states[1]
    
    message = (
        "Introduce \"<b>Si, quiero eliminar mi horario</b>\" o el comando"
        " \cancel para no hacerlo"
    )
    
    bot.send_message(chat_id=user_id, text=message, parse_mode='html')