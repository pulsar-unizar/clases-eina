import os

from app import logging
from app import middleware
from app import timetable

logger = logging.getLogger(__name__)

available_messages = {
    "H": "today",
    "HOY": "today",
    "S": "next",
    "SIGUIENTE": "next"
}

@middleware.check_user_pipeline
def plain_text(bot, update):
    """ Function than it's executed when a plain text is received. 
        If user is executing the "attempting to delete timetable file" it will
        remove the file and ends the pipeline
        
        Otherwise will display the timetable
    """
    user_id = update.effective_message.chat_id
    
    text = update.message.text

    # If user is not on a command pipeline and if it on the
    # "attempting to delete timetable file", delete the timetable file and end
    # the pipeline
    if(user_id in middleware.user_pipelines and
        middleware.user_pipelines[user_id] == middleware.user_available_states[1]):
        
        if(text != "Si, quiero eliminar mi horario"):
            message = (
                "Introduce \"Si, quiero eliminar mi horario\" o \cancel "                
                )
            bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
            return
        
        # Remove the user timetible
        user_timetable_path = "data/{}.json".format(user_id)
        os.remove(user_timetable_path) 

        # End the user delete timetable pipeline
        del middleware.user_pipelines[user_id]   
        message = ("El horario se ha borrado con éxito")
        bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
    
    # Other message than timetable deletion confirmation
    else:        
        text = text.upper()
        
        data = ()
        
        # Load the school calendar and the user timetable
        # If cannot load the user timetable, abort
        if(timetable.load_resources(user_id) == False):
            message = "No tienes ningún horario creado!"
            bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
            return

        # If message is h, hoy, s, siguiente
        if(text in available_messages):
            text = available_messages[text]
            data = timetable.get_class_day(text)
        # If message is dd/mm
        elif(text.count("/") == 1):        
            day_number, month_number = text.split("/")
            text = "custom"        
            data = timetable.get_class_day(text, day=day_number, month=month_number)

        # Otherwise
        else:
            message = (
                "El mensaje debe ser <b>(h)oy</b>, <b>(s)iguiente</b> o "
                "<b>dd/mm</b>"
                )
            bot.send_message(chat_id=user_id, text=message, parse_mode='html')
            return
        
        classes = timetable.get_classes(*data)    
        message = timetable.get_message(classes, text)
        # message = "{}, {}, {}, {}, {}".format(*data)
        bot.send_message(chat_id=user_id, text=message, parse_mode='html')
    