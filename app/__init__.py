import os
import yaml
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
# logger = logging.getLogger(__name__)

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters

from .handler import command
from .handler import message
from .handler import multimedia


config_path = os.path.join(os.environ['BASE_PATH'], 'etc', 'config.yaml')

# Load config
with open(config_path, 'r') as conf_file:
    config = yaml.load(conf_file)


def run():
    try:
        BOT_TOKEN = os.environ["BOT_TOKEN"]
    except KeyError:
        logger.critical("BOT_TOKEN ENVIRONMENT VARIABLE NOT DEFINED")
        sys.exit(1)
    
    updater = Updater(token=BOT_TOKEN)
    dispatcher = updater.dispatcher

# ------------------------------------------------------------------------------
    # Command handlers
    start_handler = CommandHandler('start', command.start)
    help_handler = CommandHandler('help', command.help)
    about_handler = CommandHandler('about', command.about)    
    
    # Message handlers
    plain_text_handler = MessageHandler(Filters.text, message.plain_text)

    # Timetable management
    add_timetable_handler = CommandHandler('add', command.add_timetable)    
    upload_timetable_handler = MessageHandler(Filters.document, multimedia.upload_timetable)
    download_timetable_handler = CommandHandler('download', command.download_timetable)
    delete_timetable_handler = CommandHandler('remove', command.delete_timetable)

    # Pipeline management
    cancel_handler = CommandHandler('cancel', command.cancel)

# ------------------------------------------------------------------------------

    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(about_handler)
    # dispatcher.add_handler(clases_handler)

    dispatcher.add_handler(plain_text_handler)

    dispatcher.add_handler(add_timetable_handler)
    dispatcher.add_handler(upload_timetable_handler)
    dispatcher.add_handler(download_timetable_handler)
    dispatcher.add_handler(delete_timetable_handler)

    dispatcher.add_handler(cancel_handler)

    # Starting the bot
    updater.start_polling()
