import os
import yaml
from app import logging

from telegram import ChatAction
from functools import wraps

logger = logging.getLogger(__name__)

whitelist_path = os.path.join(os.environ['BASE_PATH'], 'etc', 'whitelist.yaml')
# Reads the whitelist
whitelist = None
if os.path.exists(whitelist_path):
    with open(whitelist_path, 'r') as ymlfile:
        whitelist = yaml.load(ymlfile)


def only_whitelist(f):
    """ Ensure user is present in whitelist. Function wrapped should receive

        Wrapped function has to get a Bot object as first parameter (args[0]) and 
        Update object as secont one (args[1]).

        args[0] -- Bot object
        args[1] -- Update object
    """
    @wraps(f)
    def wrapper(bot, update, *args, **kwargs):
        if whitelist != None:
            if update.effective_user.id not in [u["id"] for u in whitelist]:
                message = "No esta autorizado a utilizar este bot"
                bot.send_message(
                    chat_id=update.message.chat_id, text=message)
                return
        return f(bot, update, *args, **kwargs)
    return wrapper


def use_role(role):
    """ Ensure that the user who triggered the handler have enough permission.
        Only makes sense if whitelist exists!

        Wrapped function has to get a Bot object as first parameter (args[0]) and 
        Update object as secont one (args[1]).

        role -- Minimum role that user needs
    """
    def real_decorator(f):
        @wraps(f)
        def wrapper(bot, update, *args, **kwargs):
            if whitelist != None:
                user = [u for u in whitelist if u["id"] == update.effective_user.id][0]
                if user["role"] > role:
                    message = "No esta autorizado a utilizar este comando"
                    bot.send_message(chat_id=update.message.chat_id, text=message)
            return f(bot, update, *args, **kwargs)
        return wrapper
    return real_decorator

def send_action(action):
    """Sends 'action' while processing func command."""
    def decorator(f):
        @wraps(f)
        def wrapper(bot, update, *args, **kwargs):
            bot.send_chat_action(chat_id=update.message.chat_id, action=action)
            return f(bot, update,  *args, **kwargs)
        return wrapper
    
    return decorator

# send_action alias
send_typing_action = send_action(ChatAction.TYPING)
send_upload_document_action = send_action(ChatAction.UPLOAD_DOCUMENT)
send_upload_video_action = send_action(ChatAction.UPLOAD_VIDEO)
send_upload_photo_action = send_action(ChatAction.UPLOAD_PHOTO)

# Available states:
# Not in the list -> No command in progress
# 0 -> add_timetable in progress, waiting for timetable_file
user_available_states = {    
    0: "attempting to upload timetable file",
    1: "attempting to delete timetable file"
}

user_pipelines = {}
    
def check_user_pipeline(f):
    """ Checks the user pipeline... This allows to create two or more step commands
        This means that user cannot introduce annother input but the expected
    """
    @wraps(f)
    def wrapper(bot, update, *args, **kwargs):
        user_id = update.effective_message.chat_id                                      

        if(user_id in user_pipelines):
            logger.debug("Current state of user {} is {}".format(user_id, user_pipelines[user_id]))

            # If the active pipeline is "attempting to upload timetable file" but
            # other type of message than a upload file is sent, upload_timetable
            # function is not executed so can't go on
            if(user_pipelines[user_id] == user_available_states[0] and f.__name__ != "upload_timetable"):
                logger.debug("User {} trying to upload a file".format(user_id))
                message = (
                    "Esperando un fichero con tu horario.\n"
                    "Si quieres hacer otra operación, puedes ejecutar /cancel"
                    )
                bot.send_message(chat_id=update.message.chat_id, text=message)
                return

            # If the active pipeline is "attempting to delete timetable file" but
            # other type of message than a plain_text is sent, plain_text function
            # is not executed so can't go on
            elif(user_pipelines[user_id] == user_available_states[1] and f.__name__ != "plain_text"):
                logger.debug("User {} trying to comfirm deletion of a file".format(user_id))
                message = (
                    "Esperando confirmación para borrado de horario\n"
                    "Introduce \"Si, quiero eliminar mi horario\" o \cancelar "
                    "para no borrar el horario"
                    )
                bot.send_message(chat_id=update.message.chat_id, text=message)
                return
                
        return f(bot, update,  *args, **kwargs)
    return wrapper