# Clases EINA - Telegram Bot

Bot de telegram para que los alumnos del Campus Rio Ebro puedan consultar sus 
clases. Se tienen en cuanta Festivos y los cambios de días tipicos en la EINA.

## Tabla de contenidos

- [Clases EINA - Telegram Bot](#clases-eina---telegram-bot)
  - [Tabla de contenidos](#tabla-de-contenidos)
  - [Comenzando](#comenzando)
    - [Requisitos](#requisitos)
    - [Instalacion](#instalacion)
    - [Usando Docker](#usando-docker)
    - [Usando Docker Compose](#usando-docker-compose)
  - [License](#license)

## Comenzando


**[Volver arriba](#tabla-de-contenidos)**

### Requisitos

Este proyecto esta implementado haciendo uso de la herramienta
[Pipenv][1] la cual ayuda al encapsulamiento del proyecto.

Para poder hacer uso de esto hará falta instalarlo en tu sistema. Una vez instalado
las demás dependencias estan definidas en el fichero [Pipfile](Pipfile). Para
instalarlas, simplemente ejecuta el comando `pipenv install`

- [Python Telegram Bot][2]
- [PyYAML][3]

**[Volver arriba](#tabla-de-contenidos)**

### Instalacion

**[Volver arriba](#tabla-de-contenidos)**

### Usando Docker

**[Volver arriba](#tabla-de-contenidos)**

### Usando Docker Compose


**[Volver arriba](#tabla-de-contenidos)**

## License

Este bot utiliza una licencia GPLv3 - Ver [LICENSE](LICENSE) para más detalles

**[Volver arriba](#tabla-de-contenidos)**

[1]: https://docs.pipenv.org/
[2]: https://github.com/python-telegram-bot/python-telegram-bot
[3]: http://pyyaml.org/wiki/PyYAMLDocumentation